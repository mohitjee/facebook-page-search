(function(showFavorites) {
    showFavorites.create = function(callback) {
        var showFavoritesElement = showFavorites.template();
        showFavoritesElement.addEventListener('click', function() {
            callback();
        });

        return showFavoritesElement;
    };

})(App.components.showFavorites);