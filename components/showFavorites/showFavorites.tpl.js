(function(showFavorites, htmlStringsConfig, htmlClassesConfig) {
    showFavorites.template = function() {
        var showFavoritesElement = document.createElement('button');
        showFavoritesElement.innerHTML = htmlStringsConfig.showFavorites;
        showFavoritesElement.setAttribute('class', htmlClassesConfig.showFavorites);
        return showFavoritesElement;
    };

})(App.components.showFavorites, App.config.htmlStrings, App.config.htmlClasses);