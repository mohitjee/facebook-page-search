(function(expandCollapse, htmlStringsConfig) {

    expandCollapse.template = function() {
        var expandCollapseElement = document.createElement('button');
        expandCollapseElement.innerHTML = htmlStringsConfig.expand;

        return expandCollapseElement;
    };

})(App.components.expandCollapse, App.config.htmlStrings);