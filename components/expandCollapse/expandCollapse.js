(function(expandCollapse, htmlStringsConfig) {
    expandCollapse.create = function(callback) {
        var expandCollapseElement = expandCollapse.template();

        expandCollapseElement.addEventListener('click', function() {
            expandCollapseElement.innerHTML = (expandCollapseElement.innerHTML === htmlStringsConfig.expand) ? htmlStringsConfig.collapse : htmlStringsConfig.expand;
            callback();
        });

        return expandCollapseElement;
    };

})(App.components.expandCollapse, App.config.htmlStrings);