(function(setFavorite, htmlClassesConfig, htmlStringsConfig) {
    setFavorite.create = function(callback, isFavorite) {
        var setFavoriteElement = setFavorite.template(isFavorite);

        setFavoriteElement.addEventListener('click', function() {
            setFavoriteElement.classList.toggle(htmlClassesConfig.favorite);
            setFavoriteElement.innerHTML = (setFavoriteElement.innerHTML === htmlStringsConfig.addFavorite) ? htmlStringsConfig.addedFavorite : htmlStringsConfig.addFavorite;
            callback();
        });

        return setFavoriteElement;
    };

})(App.components.setFavorite, App.config.htmlClasses, App.config.htmlStrings);