(function(setFavorite, htmlClassesConfig, htmlStringsConfig) {

    setFavorite.template = function(isFavorite) {
        var setFavoriteElement = document.createElement('button');

        if(isFavorite) {
            setFavoriteElement.setAttribute('class', htmlClassesConfig.favorite);
        }

        setFavoriteElement.innerHTML = isFavorite ? htmlStringsConfig.addedFavorite: htmlStringsConfig.addFavorite;
        return setFavoriteElement;
    };

})(App.components.setFavorite, App.config.htmlClasses, App.config.htmlStrings);