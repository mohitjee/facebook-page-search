(function(loader, messagesConfig, htmlIdsConfig) {

    loader.show =  function(message) {
            message = message || messagesConfig.defaultLoading;
            var loaderElement = loader.template(message);
            document.getElementsByTagName('body')[0].appendChild(loaderElement);
        };

    loader.hide = function() {
            var loaderElement = document.getElementById(htmlIdsConfig.loader);
            loaderElement.parentNode.removeChild(loaderElement);
    }

})(App.components.loader, App.config.messages, App.config.htmlIds);