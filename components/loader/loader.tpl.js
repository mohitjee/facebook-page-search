(function(loader, htmlIdsConfig, htmlClassesConfig) {

    loader.template = function(message) {
        var element = document.createElement('h1');

        element.setAttribute('id', htmlIdsConfig.loader);
        element.setAttribute('class', htmlClassesConfig.loader);
        element.innerHTML = '<div>'+message+'...</div>';

        return element;
    };
})(App.components.loader, App.config.htmlIds, App.config.htmlClasses);