(function(pageDetail, htmlClassesConfig) {
    pageDetail.template = function(label, value) {
        var element = document.createElement('div');
        element.setAttribute('class',htmlClassesConfig.detailRow);
        element.innerHTML = '<b>'+label+'</b> <span>'+value+'</span>';
        return element;
    };

})(App.components.pageDetail, App.config.htmlClasses);