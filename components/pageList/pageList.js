(function(page, pageList) {

    pageList.create = function(pages, favorites) {
        return pageList.template(pages, favorites);
    };

})(App.components.page, App.components.pageList);