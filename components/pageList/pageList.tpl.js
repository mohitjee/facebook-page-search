(function(page, pageList, htmlIdsConfig, htmlClassesConfig) {

    pageList.template = function(pages, favorites) {
        var pageListElement = document.createElement('div');
        pageListElement.setAttribute('id', htmlIdsConfig.pageList);
        pageListElement.setAttribute('class', htmlClassesConfig.clearfix);
        pages.forEach(function(pageData) {
            var pageElement = page.create(pageData, favorites);
            pageListElement.appendChild(pageElement);
        });

        return pageListElement;
    };

})(App.components.page, App.components.pageList, App.config.htmlIds, App.config.htmlClasses);