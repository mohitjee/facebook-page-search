(function(message) {
    message.create = function(content) {
        return message.template(content);
    };

})(App.components.message);