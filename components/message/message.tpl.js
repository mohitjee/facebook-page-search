(function(message, htmlIdsConfig) {

    message.template = function(content) {
        var messageElement = document.createElement('h3');
        messageElement.setAttribute('id', htmlIdsConfig.helperMessage);
        messageElement.innerHTML = content;

        return messageElement;
    };

})(App.components.message, App.config.htmlIds);