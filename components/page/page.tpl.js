(function(page, pageDetail, expandCollapse, setFavorite, htmlClassesConfig) {

    var getActionsElements = function(expandCallback, favoriteCallback, isFavorite) {
        var actionElements = document.createElement('div');
        actionElements.setAttribute('class', htmlClassesConfig.actions);

        var favoriteElement = setFavorite.create(favoriteCallback, isFavorite);
        var expandCollapseElement = expandCollapse.create(expandCallback);

        actionElements.appendChild(favoriteElement);
        actionElements.appendChild(expandCollapseElement);

        return actionElements;
    };

    var getDetailElements = function(detailsData) {
        var detailElements = [];

        for (var key in detailsData) {
            if(key !== 'id' && detailsData.hasOwnProperty(key)) {
                detailElements.push(pageDetail.create(key, detailsData[key]));
            }
        }

        return detailElements;
    };

    page.template = function(pageObject, favorites, getExpandCallback, getFavouriteCallback) {
        var pageElement = document.createElement('div');
        pageElement.setAttribute('id', pageObject.details.id);
        pageElement.classList.add(htmlClassesConfig.page);


        if(pageObject.favorite) {
            pageElement.classList.add(htmlClassesConfig.favoritePage);
        }

        var detailsContainerElement = document.createElement('div');
        detailsContainerElement.classList.add(htmlClassesConfig.detailsContainer);

        var detailElements = getDetailElements(pageObject.details);

        var expandCallback = getExpandCallback(pageElement, pageObject);
        var favoriteCallback = getFavouriteCallback(pageElement, pageObject, favorites);

        detailElements.forEach(function(detailElement) {
            detailsContainerElement.appendChild(detailElement);
        });

        pageElement.appendChild(detailsContainerElement);

        var actionElements = getActionsElements(expandCallback, favoriteCallback, pageObject.favorite);
        pageElement.appendChild(actionElements);

        return pageElement;
    }
})(App.components.page, App.components.pageDetail, App.components.expandCollapse, App.components.setFavorite, App.config.htmlClasses);