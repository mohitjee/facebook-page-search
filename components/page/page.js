(function(page, pageDetail, htmlClassesConfig) {

    var updatePageDetailsView = function(pageElement, pageDetails) {
        var detailElements = pageElement.querySelectorAll('.'+htmlClassesConfig.detailRow);
        var detailsContainerElement = detailElements[0].parentNode;

        Array.prototype.forEach.call( detailElements, function( node ) {
            node.parentNode.removeChild( node );
        });

        for (var key in pageDetails) {
            if(key !== 'id' && pageDetails.hasOwnProperty(key)) {
                detailsContainerElement.appendChild(pageDetail.create(key, pageDetails[key]));
            }
        }
    };

    var getAdditionalPageData = function(pageElement, pageObject) {
        if(Object.keys(pageObject.details).length <= 2) {
            pageObject.getAdditionalData(function() {updatePageDetailsView(pageElement, pageObject.details);});
        }
    };

    var getExpandCallback = function(pageElement, pageObject) {
        return function() {
            pageElement.classList.toggle(htmlClassesConfig.expanded);
            getAdditionalPageData(pageElement, pageObject);
        };
    };


    var getFavouriteCallback = function(pageElement, pageObject, favorites) {
        return function() {
            favorites.addRemoveFavorite(pageObject);
            pageElement.classList.toggle(htmlClassesConfig.favoritePage);
            pageObject.setFavorite(!pageObject.favorite);
        }
    };


    page.create = function(pageObject, favorites) {
        return page.template(pageObject, favorites, getExpandCallback, getFavouriteCallback);
    };

})(App.components.page, App.components.pageDetail, App.config.htmlClasses);