(function(root, search, message, showFavorites, htmlIdsConfig, messagesConfig) {

    var getFilters = function() {
        var filters = document.createElement('div');
        filters.setAttribute('id', htmlIdsConfig.filtersContainer);
        return filters;
    };

    root.template = function(rootElement, searchCallback, showFavoritesCallback) {
        var filters = getFilters();
        var searchElement = search.create(searchCallback);
        var showFavoritesElement = showFavorites.create(showFavoritesCallback);
        var messageElements = message.create(messagesConfig.enterQuery);

        filters.appendChild(searchElement);
        filters.appendChild(showFavoritesElement);

        rootElement.appendChild(filters);
        rootElement.appendChild(messageElements);
    };
})(App.components.root, App.components.search, App.components.message, App.components.showFavorites, App.config.htmlIds, App.config.messages);