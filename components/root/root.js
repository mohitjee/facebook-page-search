(function(root, pageList, sorter, message, htmlIdsConfig, htmlClassesConfig, messagesConfig) {
    root.init = function(id) {
        var favoritesObject = new App.models.Favorites();
        var pageListObject = new App.models.PageList();
        var rootElement = document.getElementById(id);

        var updateListView = function() {
            var pageListElement = document.getElementById(htmlIdsConfig.pageList);

            if(pageListElement) {
                pageListElement.parentNode.removeChild(pageListElement);
            }
            pageListElement = pageList.create(pageListObject.data, favoritesObject);
            rootElement.appendChild(pageListElement);
        };

        var updateSortFilterView = function() {

            var filterContainer = document.getElementById(htmlIdsConfig.filtersContainer);
            var sortElement = document.getElementById(htmlIdsConfig.sorter);
            if(sortElement) {
                sortElement.parentNode.removeChild(sortElement);
            }

            if(!pageListObject.data.length) {
                return;
            }

            sortElement = sorter.create(sortCallback);
            sortElement.setAttribute('id', htmlIdsConfig.sorter);
            filterContainer.appendChild(sortElement);
        };

        var updateMessageView = function() {
            var messageElement = document.getElementById(htmlIdsConfig.helperMessage);

            if(messageElement) {
                messageElement.parentNode.removeChild(messageElement);
            }

            if(!pageListObject.data.length) {
                messageElement = message.create(messagesConfig.noResults);
                rootElement.appendChild(messageElement);
            }
        };

        var fetchPagesCallback = function() {
            updateSortFilterView();
            updateListView();
            updateMessageView();
        };

        var sortCallback = function() {
            pageListObject.sort('name');
            updateListView();
        };

        var searchCallback = function(searchQuery) {
            pageListObject.fetchPages(searchQuery, favoritesObject.getPageIds(), fetchPagesCallback);
            rootElement.classList.remove(htmlClassesConfig.favoriteMode);
        };

        var showFavoritesCallback = function() {
            if(rootElement.className.indexOf(htmlClassesConfig.favoriteMode) <= -1) {
                rootElement.classList.toggle(htmlClassesConfig.favoriteMode);
                pageListObject.setData(favoritesObject.data);
                updateListView();
                updateMessageView();
            } else {
                var searchQuery = document.getElementById(htmlIdsConfig.search).value;
                if(searchQuery) {
                    pageListObject.fetchPages(searchQuery, favoritesObject.getPageIds(), fetchPagesCallback);
                    rootElement.classList.toggle(htmlClassesConfig.favoriteMode);
                }
            }

        };

        root.template(rootElement, searchCallback, showFavoritesCallback);
    };
})(App.components.root, App.components.pageList, App.components.sorter, App.components.message, App.config.htmlIds, App.config.htmlClasses, App.config.messages);