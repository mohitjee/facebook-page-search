(function(search, placeholdersConfig, htmlStringsConfig, htmlClassesConfig, htmlIdsConfig) {
    var getSearchInput = function() {
        var searchInputElement = document.createElement('input');
        searchInputElement.setAttribute('type', 'search');
        searchInputElement.setAttribute('id', htmlIdsConfig.search);
        searchInputElement.required = true;
        searchInputElement.setAttribute('placeholder', placeholdersConfig.searchPages);
        return searchInputElement;
    };

    var getSearchSubmit = function() {
        var searchSubmitElement = document.createElement('input');
        searchSubmitElement.setAttribute('type', 'submit');
        searchSubmitElement.setAttribute('value', htmlStringsConfig.search);
        return searchSubmitElement;
    };


    search.template = function() {
        var searchFormElement = document.createElement('form');
        searchFormElement.setAttribute('class',htmlClassesConfig.searchForm);
        searchFormElement.appendChild(getSearchInput());
        searchFormElement.appendChild(getSearchSubmit());
        return searchFormElement;
    }
})(App.components.search, App.config.placeholders, App.config.htmlStrings, App.config.htmlClasses, App.config.htmlIds);