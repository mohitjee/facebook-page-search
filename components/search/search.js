(function(search) {
    search.create = function(callback) {
        var searchElement = search.template();
        searchElement.addEventListener('submit', function(event) {
            event.preventDefault();
            var inputElement = searchElement.querySelector('input[type=search]');
            callback(inputElement.value);
        });
        return searchElement;
    };
})(App.components.search);