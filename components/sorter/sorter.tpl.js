(function(sorter, htmlClassesConfig, htmlStringsConfig) {

    sorter.template = function() {
        var sorterElement = document.createElement('button');
        sorterElement.setAttribute('class', htmlClassesConfig.sortButton);
        sorterElement.innerHTML = htmlStringsConfig.sortAsc;

        return sorterElement;
    };

})(App.components.sorter, App.config.htmlClasses, App.config.htmlStrings);