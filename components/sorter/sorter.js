(function(sorter, htmlStringsConfig) {
    sorter.create = function(callback) {
        var sorterElement = sorter.template();

        sorterElement.addEventListener('click', function() {
            sorterElement.innerHTML = sorterElement.innerHTML === htmlStringsConfig.sortAsc ? htmlStringsConfig.sortDesc : htmlStringsConfig.sortAsc;
            callback();
        });

        return sorterElement;
    };
})(App.components.sorter, App.config.htmlStrings);