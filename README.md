# **Overview** #

A vanillaJS app to search for pages present in Facebook.

## **Architecture** ##

###**App.js**###
This file is responsible for setting up the app, it initializes basic variables for all parts of the app.

###**Models**###
These contains factories for basic data types in the app such as page, pageList and favorites.

###**Components**###
All the basic elements present on the page are broken down into components. Each component contains two files.
* **<component>.js** - this file contains any event handlers on the component or any extra logic required by the component.
* **<component>.tpl.js** - this file return a DOM element for the component.

###**Config**###
Contains configuration for the App.


##**Flow in the App**##
* When the FB sdk syncs with page, the root element initializes.
* The root element create single instance of each of the models present in the app. It also adds basic components to the page like *search* and *show history*.
* When user performs search it creates the list of pages

##**Communication in the app**##
* Data flows from the parent component to the child components.
* Parent components can further interact with the child components by means of callback.
* Models can be updated via views.
* Views can optionally pass callback to model updating functions. This is done so that the view can update itself when model has changed.