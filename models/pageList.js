(function(models, Config, loader) {

    models.PageList = function() {
        this.data = [];
        this.fetchPages = function (searchQuery, favoriteIds, callback) {
            loader.show(Config.messages.gettingResults);
            var FBUrl = constructFBSearchUrl(searchQuery);
            var self = this;

            FB.api(FBUrl, function(response) {
                fetchPagesCallback(self, favoriteIds, response, callback);
            });
        };

        this.setData = function(data) {
            this.data = data;
        };

        this.sort = function(sortBy) {

            var sortAscending = function(a, b) {
                var x = a.details[sortBy]; var y = b.details[sortBy];
                if (x<y) return -1;
                else if (x>y) return 1;
                else return 0;
            };

            var sortDescending = function(a, b) {
                var x = a.details[sortBy]; var y = b.details[sortBy];
                if (x>y) return -1;
                else if (x<y) return 1;
                else return 0;
            };

            var sortFunction = (this.sortOrder === 'Descending') ? sortDescending: sortAscending;

            this.data.sort(sortFunction);

            this.sortOrder = (this.sortOrder === 'Ascending') ? 'Descending': 'Ascending';
        };
    };

    function constructFBSearchUrl(searchQuery) {
        var searchConfig = Config.search;
        return searchConfig.url
            + '?access_token=' + Config.accessToken
            + '&q=' + searchQuery
            + '&type=' + searchConfig.type;
    }

    function processResponse(response, favoriteIds) {
        return response.data.map(function(value) {
            var favorite = favoriteIds.indexOf(value.id) > -1;
            return new models.Page(value, favorite);
        });
    }

    function fetchPagesCallback(context, favoriteIds, response, callback) {
        context.sortOrder = 'Descending';
        context.data = processResponse(response, favoriteIds);
        context.sort('name');
        callback();
        loader.hide();
    }


})(App.models, App.config, App.components.loader);