(function(models) {
    models.Favorites = function() {
        this.data = [];
        this.addRemoveFavorite = function(pageObject) {

            var previousLength = this.data.length;

            this.data = this.data.filter(function(favorite) {
                return favorite.details.id !== pageObject.details.id;
            });

            if(previousLength === this.data.length) {
                this.data.push(pageObject);
            }

        };

        this.getPageIds = function() {
            return this.data.map(function(pageObject) {
                return pageObject.details.id;
            });
        }
    };

})(App.models);