(function(models, loader, Config) {
    models.Page = function(data, favorite) {
        this.details = data;
        this.favorite = favorite || false;

        this.getAdditionalData = function(callback) {
            loader.show(Config.messages.gettingPageData);
            var pageConfig = Config.page;
            var pageUrl = "/" + this.details.id
                + '?access_token=' + Config.accessToken
                + '&fields=' +pageConfig.fields;
            var self = this;
            FB.api(pageUrl, function(response) {
                getAdditionalDataCallback(self, response, callback)
            });
        };

        this.setFavorite = function(value) {
            this.favorite = value;
        };

        function getAdditionalDataCallback(context, response, callback) {
            context.details = response;
            callback();
            loader.hide();
        }
    };

})(App.models, App.components.loader, App.config);