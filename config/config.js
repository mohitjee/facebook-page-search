(function(config) {
    var FBInit = {
        appId      : '1520124311638064',
        xfbml      : true,
        version    : 'v2.5'
    };

    var search = {
        type: 'page',
        url: 'search'
    };

    var page = {
        fields: 'name,about,link'
    };

    var htmlClasses = {
        loader: 'loader',
        detailRow: 'detail-row',
        expanded: 'expanded',
        favoritePage: 'favorite-page',
        actions: 'actions',
        page: 'page',
        detailsContainer: 'details-container',
        clearfix: 'clearfix',
        favoriteMode:'favorite-mode',
        searchForm: 'search-form',
        favorite: 'favorite',
        showFavorites: 'show-favorites',
        sortButton: 'sort-button'
    };


    var htmlIds = {
        loader: 'loader',
        helperMessage: 'helperMessage',
        pageList: 'pageList',
        filtersContainer: 'filtersContainer',
        sorter: 'sorter',
        search: 'search'
    };

    var htmlStrings = {
        expand: 'Expand',
        collapse: 'Collapse',
        search: 'Search',
        addFavorite: 'Add to Favorite',
        addedFavorite: 'Added to Favorite',
        showFavorites: 'Show Favorites',
        sortAsc: 'Sort in Ascending Order',
        sortDesc: 'Sort in Descending Order'
    };

    var messages = {
        defaultLoading: 'LOADING',
        noResults: 'No results found',
        enterQuery: 'Enter a query to view results',
        initialLoading: 'Please wait while we do our thing',
        gettingResults: 'Getting your results',
        gettingPageData: 'Getting addition page information'
    };

    var placeholders = {
        searchPages: 'Search Pages...'
    };



    var accessToken = '1520124311638064|s3_XLrGaNp056BSUl8MVt678880';

    config.FBInit = FBInit;
    config.search = search;
    config.accessToken = accessToken;
    config.page = page;
    config.htmlClasses = htmlClasses;
    config.htmlIds = htmlIds;
    config.messages = messages;
    config.placeholders = placeholders;
    config.htmlStrings = htmlStrings;
})(App.config);