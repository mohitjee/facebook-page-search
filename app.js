(function(window) {
    window.App = {};

    var App = window.App;

    App.models = {};
    App.config = {};
    App.components = {
        root: {},
        loader: {},
        pageDetail: {},
        page: {},
        pageList: {},
        expandCollapse: {},
        setFavorite: {},
        search: {},
        sorter: {},
        message: {},
        showFavorites: {}
    };

})(window);